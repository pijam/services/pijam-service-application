#!/bin/bash

usage() { 
    echo "Missing argument. Usage: emulator.sh (start|stop)"
}

if [ $# -ne 1 ] || [ "$1" != "start" -a "$1" != "stop" ]; then
  usage
  exit 1
fi

if [ "$1" == "start" ] 
then
    mkdir -p .gcloud/datastore/pijam-application-service/WEB-INF
    cp src/main/webapp/WEB-INF/index.yaml .gcloud/datastore/pijam-application-service/WEB-INF
    gcloud beta emulators datastore start \
      --host-port=localhost:8095 \
      --data-dir=.gcloud/datastore/pijam-application-service \
      --consistency=1.0
    $(gcloud beta emulators datastore env-init)
else
    $(gcloud beta emulators datastore env-unset)
fi
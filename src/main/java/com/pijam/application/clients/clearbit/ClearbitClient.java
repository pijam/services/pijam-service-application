package com.pijam.application.clients.clearbit;

import java.util.Map;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(value="clearbit-api-client", url="https://company.clearbit.com/v2")
public interface ClearbitClient {
    @GetMapping(path="/companies/find")
    Map<String, ?> getCompanyInfo(
            @RequestParam("domain") String domain, 
            @RequestHeader("Authorization") String bearerToken);
}

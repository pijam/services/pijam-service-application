package com.pijam.application.clients.user;

import java.time.OffsetDateTime;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain=true)
public class UserAccount {
    private String email;
    private OffsetDateTime expires;
    private String plan;
}

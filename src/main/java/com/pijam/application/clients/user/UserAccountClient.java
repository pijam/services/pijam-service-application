package com.pijam.application.clients.user;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;

@FeignClient(value="user")
public interface UserAccountClient {
    @GetMapping(path="/users")
    UserAccount getAccount(@RequestHeader("Authorization") String bearerToken);
}

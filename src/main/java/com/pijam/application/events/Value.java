package com.pijam.application.events;

import com.pijam.application.model.Status;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@NoArgsConstructor
@Accessors(chain = true)
public class Value {
    private Integer intValue;
    private String stringValue;
    private Status statusValue;
    private Boolean booleanValue;
    
    public Value(Integer v) {
        this.intValue = v;
    }
    public Value(String v) {
        this.stringValue = v;
    }
    public Value(Status v) {
        this.statusValue = v;
    }
    public Value(Boolean v) {
        this.booleanValue = v;
    }
}
package com.pijam.application.events;

import java.util.Date;

import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Document
@Accessors(chain=true)
public class ChangeEvent {
    private Date date;
    private String field;
    private Value oldValue;
    private Value newValue;
}

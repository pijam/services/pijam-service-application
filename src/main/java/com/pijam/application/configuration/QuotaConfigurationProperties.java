package com.pijam.application.configuration;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import com.pijam.application.quota.Subscription;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Component
@ConfigurationProperties(prefix="pijam.quota")
public class QuotaConfigurationProperties {
    
    private Subscription fisher;
    private Subscription hunter;
    private Subscription sniper;
    
    public Subscription getSubscription(String plan) {
        switch (plan.toLowerCase()) {
            case "hunter": 
                return hunter;
            case "sniper": 
                return sniper;
            case "fisher": 
            default: 
                return fisher;
        }
    }
}

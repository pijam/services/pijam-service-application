package com.pijam.application.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;


@Configuration
@EnableTransactionManagement    
@EnableMongoRepositories(basePackages="com.pijam.application.repository")
public class RepositoryConfig {
    
}
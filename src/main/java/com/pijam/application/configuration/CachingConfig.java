package com.pijam.application.configuration;

import java.util.Collection;
import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.concurrent.ConcurrentMapCache;
import org.springframework.cache.concurrent.ConcurrentMapCacheManager;
import org.springframework.cache.interceptor.CacheOperationInvocationContext;
import org.springframework.cache.interceptor.CacheResolver;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.google.common.cache.CacheBuilder;
import com.google.common.collect.Lists;
import com.pijam.application.authn.UserClaims;

@Configuration
@EnableCaching
public class CachingConfig {
    
    public static final String USER_SCOPED_CACHE_RESOLVER = "userScopedCacheResolver";

    private static final int APPLICATION_CACHE_TTL_IN_DAYS = 30;
    private static final int APPLICATION_MAX_CACHE_ENTRIES = 1000;

    @Bean
    public CacheManager cacheManager() {
        return new ConcurrentMapCacheManager() {
            @Override
            protected Cache createConcurrentMapCache(final String name) {
                return new ConcurrentMapCache(name,
                    CacheBuilder.newBuilder()
                        .expireAfterWrite(APPLICATION_CACHE_TTL_IN_DAYS, TimeUnit.DAYS)
                        .maximumSize(APPLICATION_MAX_CACHE_ENTRIES).build().asMap(), false);
            }
        };
    }
    
    @Bean 
    public CacheResolver userScopedCacheResolver() {
        return new CacheResolver()  {
            @Autowired
            private CacheManager cacheManager;
            
            @Override
            public Collection<? extends Cache> resolveCaches(CacheOperationInvocationContext<?> context) {
                String email = UserClaims.get("email").asString();
                return Lists.newArrayList(cacheManager.getCache(email));
            }
        };
    }
}
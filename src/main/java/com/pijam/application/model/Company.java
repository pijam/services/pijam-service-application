package com.pijam.application.model;

import lombok.Data;

@Data
public class Company {
    private String name;
    private String logo;
    private String domain;
}

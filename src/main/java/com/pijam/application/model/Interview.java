package com.pijam.application.model;

import java.util.Date;
import java.util.List;

import javax.validation.constraints.NotNull;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.OptBoolean;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.pijam.application.utils.KeyDeserializer;
import com.pijam.application.utils.KeySerializer;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded=true)
@Accessors(chain=true)
public class Interview {
    public static final String ENTITY_NAME = "interview";
    
    @Getter
    @AllArgsConstructor
    static enum Type {
        SCREENING("Screening"),
        ON_SITE("On site"),
        PHONE("Over the phone"),
        VIDEO_CALL("Video call");
        
        private String description;
    };
    
    @Id
    @JsonSerialize(using=KeySerializer.class)
    @JsonDeserialize(using=KeyDeserializer.class)
    @EqualsAndHashCode.Include
    private ObjectId id = ObjectId.get();
    
    @NotNull
    @JsonFormat(pattern="yyyy-MM-dd'T'HH:mm:ss.SSSZ", lenient=OptBoolean.TRUE)
    private Date date;
    
    @NotNull
    private Type type;
    
    private String location;
    
    private Boolean done;

    private List<String> interviewers;
    
    private String description;

}

package com.pijam.application.model;

import java.util.Date;
import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.google.common.collect.Lists;
import com.pijam.application.events.ChangeEvent;
import com.pijam.application.utils.KeyDeserializer;
import com.pijam.application.utils.KeySerializer;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain=true)
@Document(collection=Application.ENTITY_NAME)
@EqualsAndHashCode(onlyExplicitlyIncluded=true)
public class Application {

    public static final String ENTITY_NAME = "application";
    
    public static final int DEFAULT_VALIDITY = 32; //32 days
    
    public static final String DEFAULT_SCOPE = "default";
    
    @Id
    @JsonSerialize(using=KeySerializer.class)
    @JsonDeserialize(using=KeyDeserializer.class)
    @EqualsAndHashCode.Include
    private ObjectId id;
    
    private String owner;
    
    private String scope;

    private Date applicationDate;
    
    private int validity;

    private Date lastUpdate;
    
    private String title;
    
    private String listingUrl;

    private Company company;
    
    private Contact contact;
   
    private String location;
    
    private Status status;
    
    private String description;
    
    private List<ChangeEvent> events = Lists.newArrayList();

    private List<Interview> interviews = Lists.newArrayList();
    
    private List<Note> notes = Lists.newArrayList();
}

package com.pijam.application.model.dto;

import java.util.List;

import javax.validation.constraints.NotNull;

import com.pijam.application.model.Company;
import com.pijam.application.model.Contact;
import com.pijam.application.model.Interview;
import com.pijam.application.model.Status;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
/**
 * TODO proper validation
 */
public class ApplicationDTO {
    @NotNull
    private String title;
    
    @NotNull
    private Company company;
    
    //can be null in case of cold-call applications
    private String listingUrl;

    private String description;
    
    private Contact contact;
    
    private String location;
    
    private Status status;
    
    private List<Interview> interviews;
    
    private String scope;
}

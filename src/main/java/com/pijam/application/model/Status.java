package com.pijam.application.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum Status {
    SAVED(1),
    APPLIED(2),
    IN_PROGRESS(3),
    CLOSED(4);
    
    private int id;

    public static Status of(Integer val) {
        switch (val) {
            case 1: return SAVED;
            case 2: return APPLIED;
            case 3: return IN_PROGRESS;
            case 4: return CLOSED;
            default: return null;
        }
    }
}

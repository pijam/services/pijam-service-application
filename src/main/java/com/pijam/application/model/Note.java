package com.pijam.application.model;

import java.util.Date;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.pijam.application.utils.KeyDeserializer;
import com.pijam.application.utils.KeySerializer;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain=true)
@EqualsAndHashCode(onlyExplicitlyIncluded=true)
public class Note {
    public static final String ENTITY_NAME = "note";
    
    @Id
    @JsonSerialize(using=KeySerializer.class)
    @JsonDeserialize(using=KeyDeserializer.class)
    @EqualsAndHashCode.Include
    public ObjectId id = ObjectId.get();
    
    private String content;
    
    private Date created;
    
    private Date updated;
}

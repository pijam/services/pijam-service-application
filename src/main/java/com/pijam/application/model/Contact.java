package com.pijam.application.model;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * All fields can be null. This is in particular the case when applying through a ATS.
 */
@Data
@Accessors(chain=true)
public class Contact {
    private String firstName;
    
    private String lastName;
    
    private String email;
    
    private String phone;
    
    private Boolean isRecruiter;
}

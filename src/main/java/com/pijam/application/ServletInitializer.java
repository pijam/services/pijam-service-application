package com.pijam.application;

import java.util.Arrays;

import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.context.request.RequestContextListener;

import com.pijam.application.authn.UserAccountFilter;

@Configuration
public class ServletInitializer extends SpringBootServletInitializer {
    
    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(PijamApp.class);
    }
    
    @Bean
    public FilterRegistrationBean<UserAccountFilter> userAccountFilterRegistration() {
        FilterRegistrationBean<UserAccountFilter> registration = new FilterRegistrationBean<>();
        registration.setFilter(userAccountFilter());
        registration.setUrlPatterns(Arrays.asList("/applications/*"));
        return registration;
    }

    @Bean
    public UserAccountFilter userAccountFilter() {
        return new UserAccountFilter();
    } 
    
    @Bean 
    public RequestContextListener requestContextListener(){
        return new RequestContextListener();
    } 
}

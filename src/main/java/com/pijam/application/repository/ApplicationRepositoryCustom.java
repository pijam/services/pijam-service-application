package com.pijam.application.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.pijam.application.model.Application;
import com.pijam.application.model.Status;
import com.pijam.application.quota.Usage;

import lombok.NonNull;

public interface ApplicationRepositoryCustom {
    Page<Application> query(@NonNull String email, @NonNull String scope, Boolean open, Status status, String company, Pageable pageable);

    List<Usage> getUsage(String email);
}   

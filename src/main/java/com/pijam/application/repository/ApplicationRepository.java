package com.pijam.application.repository;

import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

import com.pijam.application.model.Application;


public interface ApplicationRepository extends MongoRepository<Application, ObjectId>, ApplicationRepositoryCustom {

    List<Application> findTop1ByIdAndOwner(ObjectId id, String email);
    
    void deleteByOwner(String email);

}

package com.pijam.application.repository;

import static org.springframework.data.mongodb.core.aggregation.Aggregation.group;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.match;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.newAggregation;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.project;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import com.pijam.application.model.Application;
import com.pijam.application.model.Status;
import com.pijam.application.quota.Usage;

import lombok.NonNull;

public class ApplicationRepositoryCustomImpl implements ApplicationRepositoryCustom {

    @Autowired
    private MongoTemplate template;
    
    @Override
    public Page<Application> query(String email, @NonNull String scope, Boolean open, Status status, String company, Pageable pageable) {
        Query query = new Query();
        query.with(pageable)
             .addCriteria(Criteria.where("owner").is(email))
             .addCriteria(Criteria.where("scope").is(scope));
        
        if ( status != null ) {
            query = query.addCriteria(Criteria.where("status").is(status.toString()));
        }
        else if ( open != null ) {
            query = query.addCriteria(open == Boolean.TRUE ? 
                                 Criteria.where("status").ne("CLOSED") : 
                                 Criteria.where("status").is("CLOSED"));
        }
        
        if ( StringUtils.isNotBlank(company) ) {
            query = query.addCriteria(Criteria.where("company.name").regex(company));
        }
        
        List<Application> applications = template.find(query, Application.class);
        long count = template.count(query, Application.class);
        
        return new PageImpl<>(applications, pageable, count);
    }
    
    @Override
    public List<Usage> getUsage(String email) {
        Aggregation aggregation = newAggregation(
                match(new Criteria("owner").is(email)),
                group("$scope").count().as("count"),
                project()
                    .and("_id").as("scope")
                    .and("count").as("count")
                );
        
        AggregationResults<Usage> results  = template.aggregate(aggregation, Application.class, Usage.class);
        
        return results.getMappedResults();
    }
}

package com.pijam.application.service;

import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.pijam.application.authn.UserClaims;
import com.pijam.application.clients.user.UserAccount;
import com.pijam.application.clients.user.UserAccountClient;

@Service
public class UserAccountService {

    private static final ThreadLocal<Optional<UserAccount>> userAccountHolder = new ThreadLocal<>();
    
    @Autowired
    private UserAccountClient userAccountClient; 
    
    public Optional<UserAccount> getUserAccount() {
        Optional<UserAccount> account = userAccountHolder.get();
        if ( account == null ) {
            String token = UserClaims.getClaims().getAuthorization();
            UserAccount userAccount = userAccountClient.getAccount("Bearer " + token);
            userAccountHolder.set(Optional.of(userAccount));
        }
        return userAccountHolder.get();
    }
    
    public void clear() {
        userAccountHolder.remove();
    }
    
    public boolean isExpired() {
        OffsetDateTime expires = getUserAccount().get().getExpires();
        ZoneId zoneId = expires.toZonedDateTime().getZone();
        return expires.isBefore(OffsetDateTime.now(zoneId));
    }

    public static final String formatNamespace(String email) {
        return email
                .replace("@", "-at-")
                .replaceAll("\\.", "-dot-");
    }
}

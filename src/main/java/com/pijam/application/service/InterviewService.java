package com.pijam.application.service;

import java.util.Optional;

import javax.validation.Valid;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.stereotype.Service;

import com.pijam.application.exception.InternalPijamException;
import com.pijam.application.exception.InterviewNotFoundException;
import com.pijam.application.model.Application;
import com.pijam.application.model.Interview;
import com.pijam.application.utils.SimpleTypePropertyCopier;

@Service
public class InterviewService {
    
    @Autowired
    private ApplicationService applicationService;
    
    public Optional<Interview> get(Application application, String interviewId) {
        return application.getInterviews()
                .stream()
                .filter((i) -> StringUtils.equals(i.getId().toString(), interviewId))
                .findFirst();
    }
    
    @CacheEvict(cacheResolver="userScopedCacheResolver", allEntries=true)
    public Interview create(Application application, @Valid Interview template) {
        try {
            Interview interview = new Interview()
                    .setDate(template.getDate())
                    .setDone(template.getDone())
                    .setDescription(template.getDescription())
                    .setInterviewers(template.getInterviewers())
                    .setLocation(template.getLocation())
                    .setType(template.getType());
        
            application.getInterviews().add(interview);
            applicationService.save(application);
            return interview;
        }
        catch ( Exception e ) {
            throw new InternalPijamException("interview.create.error", e.getMessage());
        }
    }

    @CacheEvict(cacheResolver="userScopedCacheResolver", allEntries=true)
    public Interview update(Application application, String interviewId, @Valid Interview patch) {
        Interview interview = getInterview(application, interviewId);
        
        try {
            SimpleTypePropertyCopier copier = new SimpleTypePropertyCopier();
            copier.copyProperties(interview, patch);
            
            if ( CollectionUtils.isNotEmpty(patch.getInterviewers()) ) {
                interview.setInterviewers(patch.getInterviewers());
            }
            
            applicationService.save(application);
            
            return interview;
        }
        catch ( Exception e ) {
            throw new InternalPijamException("interview.update.error", e.getMessage());
        }
    }

    @CacheEvict(cacheResolver="userScopedCacheResolver", allEntries=true)
    public void delete(Application application, String interviewId) {
        Interview interview = getInterview(application, interviewId);
        application.getInterviews().remove(interview);
        applicationService.save(application);
    }
    
    private Interview getInterview(Application application, String interviewId) {
        Interview interview = application.getInterviews()
                    .stream()
                    .filter((i) -> StringUtils.equals(i.getId().toString(), interviewId))
                    .findFirst()
                    .orElseThrow(() -> new InterviewNotFoundException(application.getId().toString(), interviewId));
        return interview;
    }
}

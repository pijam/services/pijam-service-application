package com.pijam.application.service;

import java.util.Date;
import java.util.Optional;

import javax.validation.Valid;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.stereotype.Service;

import com.pijam.application.exception.NoteNotFoundException;
import com.pijam.application.model.Application;
import com.pijam.application.model.Note;

@Service
public class NoteService {
    
    @Autowired
    private ApplicationService applicationService;
    
    public Optional<Note> get(Application application, String noteId) {
        return application.getNotes()
                .stream()
                .filter((i) -> StringUtils.equals(i.getId().toString(), noteId))
                .findFirst();
    }

    @CacheEvict(cacheResolver="userScopedCacheResolver", allEntries=true)
    public Note create(Application application, @Valid Note template) {
        Date date = new Date();
        
        Note note = new Note()
                .setContent(template.getContent())
                .setCreated(date)
                .setUpdated(date);

        application.getNotes().add(note);
        applicationService.save(application);
        return note;
    }

    @CacheEvict(cacheResolver="userScopedCacheResolver", allEntries=true)
    public Note update(Application application, String noteId, @Valid Note patch) {
        Note note = getNote(application, noteId);
        
        note.setContent(patch.getContent())
            .setUpdated(new Date());
        
        applicationService.save(application);
        
        return note;
    }
    
    @CacheEvict(cacheResolver="userScopedCacheResolver", allEntries=true)
    public void delete(Application application, String noteId) {
        Note note = getNote(application, noteId);
        application.getNotes().remove(note);
        applicationService.save(application);
    }
    
    private Note getNote(Application application, String noteId) {
        Note note = application.getNotes()
                    .stream()
                    .filter((i) -> StringUtils.equals(i.getId().toString(), noteId))
                    .findFirst()
                    .orElseThrow(() -> new NoteNotFoundException(application.getId().toString(), noteId));
        return note;
    }
    
}

package com.pijam.application.service;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.pijam.application.configuration.QuotaConfigurationProperties;
import com.pijam.application.exception.QuotaExceededException;
import com.pijam.application.quota.Subscription;
import com.pijam.application.quota.Usage;
import com.pijam.application.repository.ApplicationRepository;

import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Service
@Setter
@Slf4j
public class QuotaService {
    
    @Autowired
    private ApplicationRepository applicationRepository;
    
    @Autowired
    private UserAccountService userAccountService;
    
    @Autowired
    private QuotaConfigurationProperties config;
    
    public void checkQuota(String email, String scope) throws QuotaExceededException {
        List<Usage> usage = getUsage(email);
        
        int totalScopes = usage.size();
        
        int totalApplications = usage.stream()
                .mapToInt((u) -> u.getCount())
                .sum();
        
        int applicationsInScope = usage.stream()
                .filter((u) -> StringUtils.equals(scope, u.getScope()))
                .findFirst()
                .orElseGet(() -> new Usage(scope, 0))
                .getCount();
        
        boolean scopeExists = usage.stream()
                .anyMatch((u) -> StringUtils.equals(scope, u.getScope()));
        
        String plan = userAccountService.getUserAccount().get().getPlan();
        Subscription subscription = config.getSubscription(plan);
        
        if ( subscription != null ) {
            subscription.checkQuota(totalScopes, applicationsInScope, totalApplications, scopeExists, scope);
        }
        else {
            log.error("Couldn't retrieve Subscription {}, hence quota were not checked for user {}. Config object: {}", plan, email, config);
        }
    }

    public List<Usage> getUsage(String email) {
        return applicationRepository.getUsage(email);
    }
    
}

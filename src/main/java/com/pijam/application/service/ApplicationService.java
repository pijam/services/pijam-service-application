package com.pijam.application.service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.google.common.collect.Lists;
import com.pijam.application.authn.UserClaims;
import com.pijam.application.events.ChangeEvent;
import com.pijam.application.events.Value;
import com.pijam.application.exception.ApplicationNotFoundException;
import com.pijam.application.exception.PatchApplicationException;
import com.pijam.application.model.Application;
import com.pijam.application.model.Status;
import com.pijam.application.model.dto.ApplicationDTO;
import com.pijam.application.repository.ApplicationRepository;
import com.pijam.application.utils.PropertyCopier;
import com.pijam.application.utils.SimpleTypePropertyCopier;

import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;

import static com.pijam.application.configuration.CachingConfig.USER_SCOPED_CACHE_RESOLVER;

@Slf4j
@Service
public class ApplicationService {

    @Autowired
    private ApplicationRepository applicationRepository;

    @Autowired
    private QuotaService quotaService;
    
    @Cacheable(cacheResolver=USER_SCOPED_CACHE_RESOLVER, unless="#result==null")
    public Optional<Application> get(String id) {
        List<Application> applications = applicationRepository.findTop1ByIdAndOwner(new ObjectId(id), getEmail());
        return applications.size() > 0 ? Optional.of(applications.get(0)) : Optional.empty();
    }

    @CacheEvict(cacheResolver=USER_SCOPED_CACHE_RESOLVER, allEntries=true)
    @CachePut(cacheResolver=USER_SCOPED_CACHE_RESOLVER)
    public Application create(@Valid ApplicationDTO param) {
        String scope = param.getScope();
        scope = scope == null ? Application.DEFAULT_SCOPE : scope;
        
        quotaService.checkQuota(getEmail(), scope);
        
        Date date = new Date();
        
        Application app = new Application()
                .setOwner(getEmail())
                .setApplicationDate(date)
                .setDescription(param.getDescription())
                .setCompany(param.getCompany())
                .setContact(param.getContact())
                .setLastUpdate(date)
                .setLocation(param.getLocation())
                .setScope(Application.DEFAULT_SCOPE)
                .setListingUrl(param.getListingUrl())
                .setTitle(param.getTitle())
                .setValidity(Application.DEFAULT_VALIDITY)
                .setStatus(param.getStatus() != null ? param.getStatus() : Status.APPLIED)
                .setScope(scope);

        return applicationRepository.save(app);
    }
    
    @CacheEvict(cacheResolver=USER_SCOPED_CACHE_RESOLVER, allEntries=true)
    @CachePut(cacheResolver=USER_SCOPED_CACHE_RESOLVER)
    public Application save(@Valid Application app) {
        return applicationRepository.save(app);
    }

    @CacheEvict(cacheResolver=USER_SCOPED_CACHE_RESOLVER, allEntries=true)
    public void delete(String id) {
        Optional<Application> application = get(id);

        if (!application.isPresent()) {
            log.debug("application [{}, {}] not found.", id, getEmail());
            throw new ApplicationNotFoundException(id);
        }

        applicationRepository.delete(application.get());
   }

    @CacheEvict(cacheResolver=USER_SCOPED_CACHE_RESOLVER, allEntries=true)
    public void deleteAll() {
        applicationRepository.deleteByOwner(getEmail());
    }
    
    @CacheEvict(cacheResolver=USER_SCOPED_CACHE_RESOLVER, allEntries=true)
    @CachePut(cacheResolver=USER_SCOPED_CACHE_RESOLVER, unless="#result==null")
    public Application patch(Application application, ApplicationDTO patch, List<String> forceNull) {
        Date date = new Date();

        PropertyCopier propertyUtil = new SimpleTypePropertyCopier(forceNull);

        Status newStatus = patch.getStatus();
        
        Application app = new Application()
                .setTitle(patch.getTitle())
                .setDescription(patch.getDescription())
                .setLocation(patch.getLocation())
                .setListingUrl(patch.getListingUrl())
                .setStatus(newStatus);
        
        Status oldStatus = application.getStatus();
        if ( newStatus != null && newStatus != oldStatus ) {
            if ( application.getEvents() == null ) {
                application.setEvents(Lists.newArrayList());
            }

            application.getEvents()
                .add(new ChangeEvent()
                        .setField("status")
                        .setDate(date)
                        .setNewValue(new Value(newStatus))
                        .setOldValue(new Value(oldStatus)));
        }
            
        try {
            propertyUtil.copyProperties(application, app);

            propertyUtil.copyProperties(application.getCompany(), patch.getCompany());
            propertyUtil.copyProperties(application.getContact(), patch.getContact());
            
            application.setLastUpdate(date);
            
            Application result =  applicationRepository.save(application);
            
            return result;
        } 
        catch (Exception e) {
            log.error("Unable to patch application {}", application.getId(), e);
            throw new PatchApplicationException(application.getId());
        }
    }
    
    @Cacheable(cacheResolver=USER_SCOPED_CACHE_RESOLVER, unless="#result==null or #result.getNumberOfElements()==0")
    public Page<Application> findAll(@NonNull String scope, Boolean open, Status status, String company, Pageable pageable) {
        return applicationRepository.query(getEmail(), scope, open, status, company, pageable);
    }

    private String getEmail() {
        return UserClaims.get("email").asString();
    }
    
}





package com.pijam.application.controller;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.TimeUnit;

import javax.validation.Valid;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.cache.CacheBuilder;
import com.google.common.collect.Maps;
import com.pijam.application.authn.UserClaims;
import com.pijam.application.clients.clearbit.ClearbitClient;
import com.pijam.application.exception.ApplicationNotFoundException;
import com.pijam.application.exception.InterviewNotFoundException;
import com.pijam.application.exception.NoteNotFoundException;
import com.pijam.application.model.Application;
import com.pijam.application.model.Company;
import com.pijam.application.model.Interview;
import com.pijam.application.model.Note;
import com.pijam.application.model.Status;
import com.pijam.application.model.dto.ApplicationDTO;
import com.pijam.application.quota.Usage;
import com.pijam.application.service.ApplicationService;
import com.pijam.application.service.InterviewService;
import com.pijam.application.service.NoteService;
import com.pijam.application.service.QuotaService;

import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
@RequestMapping(path="/applications")
public class ApplicationController {
    
    @Autowired
    private ApplicationService applicationService;
    
    @Autowired
    private NoteService noteService;
    
    @Autowired 
    private InterviewService interviewService;
    
    @Autowired
    private ClearbitClient clearbitClient;
    
    @Value("${clearbit.api.key}")
    private String clearbitPrivateKey;
    
    @Autowired
    private QuotaService quotaService;
    
    private ConcurrentMap<Object, Object> companyCache = CacheBuilder.newBuilder()
            .expireAfterWrite(30, TimeUnit.MINUTES)
            .maximumSize(100).build().asMap();
    
    // ------------------------------ 
    // Application controller methods
    // ------------------------------
    
    @PostMapping(
            consumes=MediaType.APPLICATION_JSON_VALUE, 
            produces=MediaType.APPLICATION_JSON_VALUE)
    public Application create(@Valid @RequestBody ApplicationDTO param) {
        return applicationService.create(param);
    }
    
    @GetMapping(path="/{id}", produces=MediaType.APPLICATION_JSON_VALUE)
    public Application get(@PathVariable("id") String id) {
        return applicationService.get(id).orElseThrow(() -> new ApplicationNotFoundException(id));
    }
    
    /**
     * @todo use PageableDefault, SortDefault instead of size, page, sortBy and dir params
     */
    @GetMapping(produces=MediaType.APPLICATION_JSON_VALUE)
    public Page<Application> findAll(
            @RequestParam(defaultValue="50") int size, @RequestParam(defaultValue="0") int page, 
            @RequestParam(defaultValue="lastUpdate") String sortBy, @RequestParam(defaultValue="desc") String dir,
            @RequestParam(name="scope", defaultValue="default") String scope,
            @RequestParam(name="open", required=false) Boolean open,
            @RequestParam(name="status", required=false) Status status,
            @RequestParam(name="company", required=false) String company) {
        Sort sort = Sort.by(Direction.fromString(dir.toUpperCase()),  sortBy);
        Pageable pageable = PageRequest.of(page, size, sort);
        return applicationService.findAll(scope, open, status, company, pageable);
    }

    @PatchMapping(path="/{id}", 
            consumes=MediaType.APPLICATION_JSON_VALUE, 
            produces=MediaType.APPLICATION_JSON_VALUE)
    public Application update(
            @PathVariable("id") String id, @RequestBody ApplicationDTO patch, 
            @RequestParam(name="forceNull", required=false) List<String> forceNull) {
        Application application = applicationService.get(id).orElseThrow(() -> new ApplicationNotFoundException(id));
        return applicationService.patch(application, patch, forceNull);
    }
    
    @DeleteMapping(path="/{id}")
    @ResponseStatus(code=HttpStatus.NO_CONTENT)
    public void deleteApplication(@PathVariable String id) {
        applicationService.get(id).orElseThrow(() -> new ApplicationNotFoundException(id));   
        applicationService.delete(id);
    }
    
    @DeleteMapping
    @ResponseStatus(code=HttpStatus.NO_CONTENT)
    public void deleteApplications() {
        applicationService.deleteAll();
    }
    
    // ----------------------
    // company info retrieval
    // ----------------------
    @SuppressWarnings("unchecked")
    @GetMapping(path="/{id}/company")
    public Map<String, ?> getCompanyInfo(@PathVariable String id) {
        Application application = applicationService.get(id).orElseThrow(() -> new ApplicationNotFoundException(id));
        Company company = application.getCompany();
        if ( company == null || StringUtils.isBlank(company.getDomain()) ) {
            return Maps.newHashMap();
        }
        try {
            String domain = company.getDomain();
            
            Map<String, Object> value = (Map<String, Object>) companyCache.get(domain);
            
            if ( value == null ) { 
                value = (Map<String, Object>) clearbitClient.getCompanyInfo(domain, "Bearer " + clearbitPrivateKey);
                companyCache.put(domain, value);
            }
            
            return value;
        }
        catch ( Exception e ) {
            log.error("Unable to fetch company {} info from clearbit: {}", 
                      company.getDomain(), e.getMessage());
            return Maps.newHashMap();
        }
    }
    
    // ----------------------- 
    // Note controller methods
    // -----------------------
    
    @GetMapping(path="/{id}/notes", produces=MediaType.APPLICATION_JSON_VALUE)
    public List<Note> getNotes(@PathVariable String id) {
        Application application = applicationService.get(id).orElseThrow(() -> new ApplicationNotFoundException(id));
        return application.getNotes();
    }
    
    @GetMapping(path="/{id}/notes/{noteId}", produces=MediaType.APPLICATION_JSON_VALUE)
    public Note getNote(@PathVariable("id") String id, @PathVariable("noteId") String noteId) {
        Application application = applicationService.get(id).orElseThrow(() -> new ApplicationNotFoundException(id));   
        return noteService.get(application, noteId).orElseThrow(
                () -> new NoteNotFoundException(id, noteId));
    }
    
    @PostMapping(path="/{id}/notes", produces=MediaType.APPLICATION_JSON_VALUE)
    public Note addNote(@PathVariable("id") String id, @RequestBody Note note) {
        Application application = applicationService.get(id).orElseThrow(() -> new ApplicationNotFoundException(id));   
        return noteService.create(application, note);
    }

    @PutMapping(path="/{id}/notes/{noteId}", produces=MediaType.APPLICATION_JSON_VALUE)
    public Note updateNote(@PathVariable("id") String id, @PathVariable("noteId") String noteId, @Valid @RequestBody Note note) {
        Application application = applicationService.get(id).orElseThrow(() -> new ApplicationNotFoundException(id));   
        return noteService.update(application, noteId, note);
    }

    @DeleteMapping(path="/{id}/notes/{noteId}")
    @ResponseStatus(code=HttpStatus.NO_CONTENT)
    public void deleteNote(@PathVariable("id") String id, @PathVariable("noteId") String noteId) {
        Application application = applicationService.get(id).orElseThrow(() -> new ApplicationNotFoundException(id));   
        noteService.delete(application, noteId);
    }

    // ---------------------------- 
    // Interview controller methods
    // ----------------------------
    
    @GetMapping(path="/{id}/interviews", produces=MediaType.APPLICATION_JSON_VALUE)
    public List<Interview> getInterviews(@PathVariable String id) {
        Application application = applicationService.get(id).orElseThrow(() -> new ApplicationNotFoundException(id));
        return application.getInterviews();
    }
    
    @PostMapping(path="/{id}/interviews", produces=MediaType.APPLICATION_JSON_VALUE)
    public Interview addInterview(@PathVariable("id") String id, @Valid @RequestBody Interview interview) {
        Application application = applicationService.get(id).orElseThrow(() -> new ApplicationNotFoundException(id));   
        return interviewService.create(application, interview);
    }
    
    @GetMapping(path="/{id}/interviews/{interviewId}", produces=MediaType.APPLICATION_JSON_VALUE)
    public Interview getInterview(@PathVariable("id") String id, @PathVariable("interviewId") String interviewId) {
        Application application = applicationService.get(id).orElseThrow(() -> new ApplicationNotFoundException(id));
        return interviewService.get(application, interviewId).orElseThrow(
                () -> new InterviewNotFoundException(id, interviewId));
    }

    @PutMapping(path="/{id}/interviews/{interviewId}", produces=MediaType.APPLICATION_JSON_VALUE)
    public Interview updateInterview(@PathVariable("id") String id, @PathVariable("interviewId") String interviewId, @Valid @RequestBody Interview interview) {
        Application application = applicationService.get(id).orElseThrow(() -> new ApplicationNotFoundException(id));
        return interviewService.update(application, interviewId, interview);
    }
    
    @DeleteMapping(path="/{id}/interviews/{interviewId}")
    @ResponseStatus(code=HttpStatus.NO_CONTENT)
    public void deleteInterview(@PathVariable("id") String id, @PathVariable("interviewId") String interviewId) {
        Application application = applicationService.get(id).orElseThrow(() -> new ApplicationNotFoundException(id));
        interviewService.delete(application, interviewId);
    }

    // ------------------------
    // Quota controller methods
    // ------------------------
    
    @GetMapping(path="/usage", produces=MediaType.APPLICATION_JSON_VALUE)
    public List<Usage> getUsage() {
        String email = UserClaims.get("email").asString();
        return quotaService.getUsage(email);
    }
}



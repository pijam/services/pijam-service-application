package com.pijam.application.authn;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.cors.CorsUtils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.pijam.application.clients.user.UserAccount;
import com.pijam.application.exception.AccountExpiredException;
import com.pijam.application.exception.PijamException;
import com.pijam.application.exception.PijamExceptionHandler;
import com.pijam.application.exception.UserNotFoundException;
import com.pijam.application.service.UserAccountService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class UserAccountFilter implements Filter {
    
    @Autowired 
    private UserAccountService accountService;
    
    @Autowired
    private PijamExceptionHandler handler;
    
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        try {
            if ( !CorsUtils.isPreFlightRequest((HttpServletRequest) request) ) {
                UserClaims claims = UserClaims.init((HttpServletRequest) request);
                
                if (claims == null ) {
                    ((HttpServletResponse) response).sendError(HttpStatus.UNAUTHORIZED.value());
                    return;
                }
                
                checkUserAccount();
            }
            chain.doFilter(request, response);
        }
        catch ( PijamException ex ) {
            handlePijamException((HttpServletRequest) request, (HttpServletResponse) response, ex);
            return;
        }
        finally {
            accountService.clear();
            UserClaims.reset();
        }
    }
    
    private void checkUserAccount() {
        UserAccount account = accountService.getUserAccount().orElseThrow(() -> new UserNotFoundException());
        if ( accountService.isExpired() ) {
            throw new AccountExpiredException(account.getExpires());
        }
    }
    
    @SuppressWarnings("unchecked")
    private void handlePijamException(HttpServletRequest request, HttpServletResponse response, PijamException exception) throws JsonProcessingException, IOException {
        log.error("Error while verifying user account", exception);
        Map<String, Object> map = handler.handlePijamException(request, response, exception);
        response.setContentType(MediaType.APPLICATION_JSON_VALUE);
        Object error = map.getOrDefault(PijamExceptionHandler.ERROR, new HashMap<String, Object>());
        String key = null;
        if ( error instanceof Map ) {
            key = (String) ((Map<String, Object>) error).get("key");
        }
        response.sendError((Integer) map.get(PijamExceptionHandler.STATUS), key);
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }
    
    @Override
    public void destroy() {

    }
    
}

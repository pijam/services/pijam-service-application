package com.pijam.application.quota;

import com.pijam.application.exception.QuotaExceededException;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Subscription {
    private int scopes;
    private int applications;
    private int applicationsPerScope;
    
    public void checkQuota(int totalScopes, int applicationsInScope, int totalApplications, boolean scopeExists, String requestedScope) throws QuotaExceededException {
        if ( totalScopes >= this.scopes && !scopeExists ||
             applicationsInScope >= this.applicationsPerScope ||  
             totalApplications >= this.applications ) {
            throw new QuotaExceededException(this, requestedScope, totalScopes, applicationsInScope, totalApplications);
        }
    }
}
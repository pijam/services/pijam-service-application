package com.pijam.application.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@SuppressWarnings("serial")
@ResponseStatus(value=HttpStatus.INTERNAL_SERVER_ERROR)
public class InternalPijamException extends PijamException {
    public InternalPijamException(String key, String message) {
        super(key, message);
    }
}

package com.pijam.application.exception;

import java.util.Map;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import lombok.Getter;
import lombok.Setter;

@SuppressWarnings("serial")
@ResponseStatus(value=HttpStatus.NOT_FOUND)
public class InterviewNotFoundException extends PijamException {
    @Getter
    @Setter
    private String applicationId;
    
    @Getter
    @Setter
    private String interviewId;
    
    public InterviewNotFoundException(String applicationId, String interviewId) {
        super("application.error.notfound", "Requested interview not found");
        this.applicationId = applicationId;
        this.interviewId = interviewId;
    }
    
    @Override
    protected Map<String, Object> getErrorInfo() {
        Map<String, Object> map = super.getErrorInfo();
        map.put("applicationId", applicationId);
        map.put("interviewId", interviewId);
        return map;
    }
    
}

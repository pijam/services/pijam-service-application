package com.pijam.application.exception;

import java.time.OffsetDateTime;
import java.util.Map;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import lombok.Getter;
import lombok.Setter;

@SuppressWarnings("serial")
@ResponseStatus(value=HttpStatus.FORBIDDEN)
public class AccountExpiredException extends PijamException {
    @Getter 
    @Setter
    private OffsetDateTime since;
    
    public AccountExpiredException(OffsetDateTime odt) {
        super("account.error.expired", "Account is expired");
        this.since = odt;
    }
    
    @Override
    protected Map<String, Object> getErrorInfo() {
        Map<String, Object> map = super.getErrorInfo();
        map.put("since", since);
        return map;
    }
}

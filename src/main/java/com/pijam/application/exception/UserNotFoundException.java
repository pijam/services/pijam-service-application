package com.pijam.application.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@SuppressWarnings("serial")
@ResponseStatus(value=HttpStatus.FORBIDDEN)
public class UserNotFoundException extends PijamException {
    
    public UserNotFoundException() {
        super("user.error.notregistered", "User not yet registered");
    }
}

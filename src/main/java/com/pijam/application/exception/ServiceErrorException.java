package com.pijam.application.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@SuppressWarnings("serial")
@ResponseStatus(value=HttpStatus.SERVICE_UNAVAILABLE)
public class ServiceErrorException extends PijamException {
    public ServiceErrorException(String service) {
        super(
            String.format("service.%s.unavailable", service.toLowerCase()), 
            String.format("%s service not available", service));
    }
}

package com.pijam.application.exception;

import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.client.HttpClientErrorException.MethodNotAllowed;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@ControllerAdvice
public class PijamExceptionHandler {

    public static final Map<Class<? extends Exception>, HttpStatus> map = new HashMap<>();
    static {
        map.put(MethodArgumentNotValidException.class, HttpStatus.BAD_REQUEST);
        map.put(MethodNotAllowed.class, HttpStatus.METHOD_NOT_ALLOWED);
    }
    
	public static final String ERROR = "error";
	public static final String TIMESTAMP = "timestamp";
	public static final String STATUS = "status";
	public static final String RESOURCE = "resource";

    @ResponseBody
	@ExceptionHandler(PijamException.class)
	public Map<String, Object> handlePijamException(
	        HttpServletRequest request, 
	        HttpServletResponse response, 
	        PijamException ex) {
        return handle(request, response, ex, ex.describe());
	}
    
	@ResponseBody
    @ExceptionHandler(Exception.class)
    public Map<String, Object> handleGenericException(
            HttpServletRequest request, 
            HttpServletResponse response, 
            Exception ex) {
	    String message = map.containsKey(ex.getClass()) ? ex.getMessage() : "error";
	    return handle(request, response, ex, message);
    }
	
	private Map<String, Object> handle(HttpServletRequest request, HttpServletResponse response, Exception ex, Object error) {
        log.error("Error while processing request", ex);

        Map<String, Object> map = new LinkedHashMap<>();
        
        int status = getHttpStatus(ex);
        
        map.put(TIMESTAMP, new Date().getTime());
        map.put(RESOURCE, request.getServletPath());
        map.put(STATUS, status);
        map.put(ERROR, error);

        response.setStatus(status);
        
        return map;
    }
    
    private int getHttpStatus(Exception ex) {
		HttpStatus status = map.getOrDefault(ex, HttpStatus.INTERNAL_SERVER_ERROR);

		if ( ex.getClass().isAnnotationPresent(ResponseStatus.class) ) {
		    status = ex.getClass().getAnnotation(ResponseStatus.class).value();
		}
        
        
		return status.value();
    }

	
}
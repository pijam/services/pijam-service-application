package com.pijam.application.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.pijam.application.quota.Subscription;

@SuppressWarnings("serial")
@ResponseStatus(value=HttpStatus.FORBIDDEN)
public class QuotaExceededException extends PijamException {
    
    public QuotaExceededException(Subscription subscription, String requestedSCope, int totalScopes, int applicationsInScope, int totalApplications) {
        super("quota.exceeded", constructMessage(subscription, requestedSCope, totalScopes, applicationsInScope, totalApplications));
    }

    private static String constructMessage(Subscription subscription, String requestedScope, int totalScopes, int applicationsInScope, int totalApplications) {
        StringBuilder builder = new StringBuilder("Requested scope '")
                .append(requestedScope)
                .append(applicationsInScope > 0 ? "' already exists. " : "' is new. Usage:")
                .append("\n");
        
        if ( applicationsInScope > 0 ) {
            builder = builder
                    .append("   - applications in scope: ")
                    .append(applicationsInScope)
                    .append("/")
                    .append(subscription.getApplicationsPerScope())
                    .append("\n");
        }
        
        builder = builder
                .append("   - total scopes: ")
                .append(totalScopes)
                .append("/")
                .append(subscription.getScopes())
                .append("\n");
                
        builder = builder
                .append("   - total applications: ")
                .append(totalApplications)
                .append("/")
                .append(subscription.getApplications())
                .append("\n");
                
        return builder.toString();
    }
}

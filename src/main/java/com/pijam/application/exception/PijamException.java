package com.pijam.application.exception;

import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.commons.collections.MapUtils;

import lombok.Getter;
import lombok.Setter;

@SuppressWarnings("serial")
public abstract class PijamException extends RuntimeException {
    @Getter @Setter
    private String key;
    
    public PijamException(String key, String message) {
        super(message);
        this.key = key;
    }
    
    public final Map<String, Object> describe() {
        Map<String, Object> map = new LinkedHashMap<>();
        map.put("key", key);
        map.put("message", getMessage());
        
        Map<String, Object> errorInfo = getErrorInfo();
        if ( MapUtils.isNotEmpty(errorInfo) ) {
            map.put("error_info", errorInfo);
        }
        
        return map; 
    }
    
    protected Map<String, Object> getErrorInfo() {
        return new LinkedHashMap<>();
    }
}

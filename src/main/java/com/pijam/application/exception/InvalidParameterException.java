package com.pijam.application.exception;

import java.util.Arrays;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@SuppressWarnings("serial")
@ResponseStatus(value=HttpStatus.BAD_REQUEST)
public class InvalidParameterException extends PijamException {
    
    public static final String ONLY_ONE_OF = "only.one.of";

    public InvalidParameterException(String key, String... params) {
        super(key, String.format("Invalid parameters. Only one of %s is allowed", Arrays.toString(params)));
    }
}

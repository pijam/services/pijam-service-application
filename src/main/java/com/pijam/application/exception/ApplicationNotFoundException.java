package com.pijam.application.exception;

import java.util.Map;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import lombok.Getter;
import lombok.Setter;

@SuppressWarnings("serial")
@ResponseStatus(value=HttpStatus.NOT_FOUND)
public class ApplicationNotFoundException extends PijamException {
    @Getter
    @Setter
    private String id;
    
    public ApplicationNotFoundException(String id) {
        super("application.error.notfound", "Requested application not found");
        this.id = id;
    }
    
    @Override
    protected Map<String, Object> getErrorInfo() {
        Map<String, Object> map = super.getErrorInfo();
        map.put("id", id);
        return map;
    }
    
}

package com.pijam.application.exception;

import java.util.Map;

import org.bson.types.ObjectId;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import lombok.Getter;
import lombok.Setter;

@SuppressWarnings("serial")
@ResponseStatus(value=HttpStatus.INTERNAL_SERVER_ERROR)
public class PatchApplicationException extends PijamException {
    @Getter
    @Setter
    private ObjectId id;
    
    public PatchApplicationException(ObjectId id) {
        super("application.error.patch.internal", "An error occured while updating the application");
        this.id = id;
    }
    
    @Override
    protected Map<String, Object> getErrorInfo() {
        Map<String, Object> map = super.getErrorInfo();
        map.put("id", id);
        return map;
    }
}

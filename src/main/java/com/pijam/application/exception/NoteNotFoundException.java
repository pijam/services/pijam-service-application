package com.pijam.application.exception;

import java.util.Map;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import lombok.Getter;
import lombok.Setter;

@SuppressWarnings("serial")
@ResponseStatus(value=HttpStatus.NOT_FOUND)
public class NoteNotFoundException extends PijamException {
    @Getter
    @Setter
    private String applicationId;
    
    @Getter
    @Setter
    private String noteId;
    
    public NoteNotFoundException(String applicationId, String noteId) {
        super("application.error.notfound", "Requested note not found");
        this.applicationId = applicationId;
        this.noteId = noteId;
    }
    
    @Override
    protected Map<String, Object> getErrorInfo() {
        Map<String, Object> map = super.getErrorInfo();
        map.put("applicationId", applicationId);
        map.put("noteId", noteId);
        return map;
    }
    
}

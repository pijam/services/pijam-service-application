package com.pijam.application.utils;

import java.util.List;

import com.google.common.collect.Lists;

public class SimpleTypePropertyCopier extends PropertyCopier {

    public SimpleTypePropertyCopier(final List<String> forceNull) {
        super(new CopyPredicate(forceNull));
    }

    public SimpleTypePropertyCopier() {
        this(Lists.newArrayList());
    }

}

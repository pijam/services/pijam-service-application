package com.pijam.application.utils;
import java.lang.reflect.InvocationTargetException;
import java.util.function.BiPredicate;

import org.apache.commons.beanutils.FluentPropertyBeanIntrospector;
import org.apache.commons.beanutils.PropertyUtilsBean;

import lombok.NonNull;

public class PropertyCopier extends PropertyUtilsBean {
    
    private BiPredicate<String, Object> predicate;
    
    public PropertyCopier(@NonNull BiPredicate<String, Object> predicate) {
        this.predicate = predicate;
        addBeanIntrospector(new FluentPropertyBeanIntrospector());
    }
    
    @Override
    public void copyProperties(Object dest, Object orig) throws IllegalAccessException, InvocationTargetException, NoSuchMethodException {
        if ( orig != null && dest != null ) {
            super.copyProperties(dest, orig);
        }
    }
    
    @Override
    public void setSimpleProperty(Object dest, String name, Object value)  throws IllegalAccessException, InvocationTargetException, NoSuchMethodException {
        if( predicate.test(name, value) ) {
            super.setSimpleProperty(dest, name, value);
        }
    }

}
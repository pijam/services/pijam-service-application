package com.pijam.application.utils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.function.BiPredicate;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.ClassUtils;
import org.apache.commons.lang3.StringUtils;

import com.google.common.collect.Lists;

public class CopyPredicate implements BiPredicate<String, Object> {

    private List<String> forceNull = new ArrayList<>();

    public CopyPredicate(List<String> forceNull) {
        this.forceNull = CollectionUtils.isEmpty(forceNull) ? Lists.newArrayList() : forceNull;
    }

    @Override
    public boolean test(String name, Object value) {
        // always copy value if forceNull contains property name
        if ( this.forceNull.contains(name) ) {
            return true;
        }

        // otherwise skip null values
        if (value == null ) {
            return false;
        }
        
        // copy non empty Collections
        if (value.getClass().isInstance(Collection.class)) {
            return CollectionUtils.isNotEmpty((Collection<?>) value);
        }

        // copy non blank Strings
        if (String.class.isInstance(value)) {
            return StringUtils.isNotBlank((String) value);
        }
        
        // always copy Enums
        if (value.getClass().isEnum()) {
            return true;
        }

        // always copy Date
        if (Date.class.isInstance(value)) {
            return true;
        }
        
        // never copy non primitive
        if (!ClassUtils.isPrimitiveOrWrapper(value.getClass())) {
            return false;
        }

        // copying only Non default values doesn't really work (make it impossible to reset), so let's just return true 
        // return !Objects.equal(o, Defaults.defaultValue(Primitives.unwrap(o.getClass())));

        return true;
    }
}
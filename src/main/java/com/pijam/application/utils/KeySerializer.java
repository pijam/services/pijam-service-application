package com.pijam.application.utils;

import java.io.IOException;

import org.bson.types.ObjectId;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

public class KeySerializer extends JsonSerializer<ObjectId> {
    @Override
    public void serialize(ObjectId key, JsonGenerator gen, SerializerProvider serializers) throws IOException {
        if ( key == null ) {
            return;
        }
        gen.writeString(key.toString());
    }

}

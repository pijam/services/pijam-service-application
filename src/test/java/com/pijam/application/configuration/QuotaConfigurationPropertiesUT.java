package com.pijam.application.configuration;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.test.context.ConfigFileApplicationContextInitializer;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.pijam.application.quota.Subscription;

@ExtendWith(SpringExtension.class)
@EnableConfigurationProperties
@ContextConfiguration(
        initializers=ConfigFileApplicationContextInitializer.class, 
        classes=QuotaConfigurationProperties.class)
class QuotaConfigurationPropertiesUT {

    @Autowired
    private QuotaConfigurationProperties properties;
    
    @Test
    void testInitialization() {
        checkSubscription(properties.getSniper(), -1, -1, -1);
        checkSubscription(properties.getHunter(), 2, 30, 50);
        checkSubscription(properties.getFisher(), 1, 25, 25);
    }

    @Test
    void testGetSubscription() {
        checkSubscription(properties.getSubscription("sniper"), -1, -1, -1);
        checkSubscription(properties.getSubscription("hunter"), 2, 30, 50);
        checkSubscription(properties.getSubscription("fisher"), 1, 25, 25);
    }

    private void checkSubscription(Subscription subscription, int scopes, int applicationsPerScope, int applications) {
        assertNotNull(subscription);
        assertEquals(scopes, subscription.getScopes());
        assertEquals(applicationsPerScope, subscription.getApplicationsPerScope());
        assertEquals(applications, subscription.getApplications());
    }
    
}

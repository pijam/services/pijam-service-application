package com.pijam.application.service;

import static org.mockito.Mockito.when;

import java.time.OffsetDateTime;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.test.context.ConfigFileApplicationContextInitializer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.google.common.collect.Lists;
import com.pijam.application.clients.user.UserAccount;
import com.pijam.application.configuration.QuotaConfigurationProperties;
import com.pijam.application.quota.Usage;
import com.pijam.application.repository.ApplicationRepository;

@ExtendWith({SpringExtension.class, MockitoExtension.class})
@EnableConfigurationProperties
@ComponentScan(basePackages= {"com.pijam.application"})
@ContextConfiguration(
        initializers=ConfigFileApplicationContextInitializer.class, 
        classes= {QuotaConfigurationProperties.class})
class QuotaServiceUT {

    @Autowired
    private QuotaService service;

    @Mock
    private ApplicationRepository repository;
    
    
    @Mock
    private UserAccountService userAccountService; 
    
    
    @BeforeEach
    public void setup() {
        service.setUserAccountService(userAccountService);
        service.setApplicationRepository(repository);
        
        when(userAccountService.getUserAccount()).
        thenReturn(Optional.of(new UserAccount()
                .setEmail("email")
                .setExpires(OffsetDateTime.now().plusMonths(1))
                .setPlan("fisher")));
        
        when(repository.getUsage("email")).thenReturn(Lists.newArrayList(new Usage("default", 5)));
    }
    
    @Test
    void test() {
        service.checkQuota("email", "default");
    }

}

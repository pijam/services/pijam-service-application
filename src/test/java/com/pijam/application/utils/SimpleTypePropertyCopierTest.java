package com.pijam.application.utils;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.lang.reflect.InvocationTargetException;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.google.common.collect.Lists;

class SimpleTypePropertyCopierTest {
    private SimpleTypePropertyCopier copier;
    
    private TestBean dest;
    private TestBean orig;
    
    @BeforeEach
    void setup() {
        copier = new SimpleTypePropertyCopier();
        dest = new TestBean();
        orig = new TestBean();
    }
    
    @Test
    void testCopyNonBlankStringWhenDestIsNotNull() throws IllegalAccessException, InvocationTargetException, NoSuchMethodException {
        dest.setStringProperty("dest");
        orig.setStringProperty("orig");
        copier.copyProperties(dest, orig);
        assertEquals("orig", dest.getStringProperty());
    }

    
    @Test
    void testCopyNonBlankStringWhenDestIsNull() throws IllegalAccessException, InvocationTargetException, NoSuchMethodException {
        orig.setStringProperty("orig");
        copier.copyProperties(dest, orig);
        assertEquals("orig", dest.getStringProperty());
    }
    
    @Test
    void testCopyBlankString() throws IllegalAccessException, InvocationTargetException, NoSuchMethodException {
        dest.setStringProperty("dest");
        orig.setStringProperty(" ");
        copier.copyProperties(dest, orig);
        assertEquals("dest", dest.getStringProperty());
    }
    
    @Test
    void testCopyNullString() throws IllegalAccessException, InvocationTargetException, NoSuchMethodException {
        dest.setStringProperty("dest");
        copier.copyProperties(dest, orig);
        assertEquals("dest", dest.getStringProperty());
    }
    
    @Test
    void testForceCopyBlankString() throws IllegalAccessException, InvocationTargetException, NoSuchMethodException {
        SimpleTypePropertyCopier copier = new SimpleTypePropertyCopier(Lists.newArrayList("stringProperty"));
        dest.setStringProperty("dest");
        orig.setStringProperty(" ");
        copier.copyProperties(dest, orig);
        assertEquals(" ", dest.getStringProperty());
    }
    
    @Test
    void testForceCopyNullString() throws IllegalAccessException, InvocationTargetException, NoSuchMethodException {
        SimpleTypePropertyCopier copier = new SimpleTypePropertyCopier(Lists.newArrayList("stringProperty"));
        dest.setStringProperty("dest");
        orig.setStringProperty(null);
        copier.copyProperties(dest, orig);
        assertEquals(null, dest.getStringProperty());
    }
    
    @Test
    void testCopyIntWhenDestIsNonDefault() throws IllegalAccessException, InvocationTargetException, NoSuchMethodException {
        dest.setIntProperty(1);
        orig.setIntProperty(2);
        copier.copyProperties(dest, orig);
        assertEquals(2, dest.getIntProperty());
    }
    
    @Test
    void testCopyDefaultInt() throws IllegalAccessException, InvocationTargetException, NoSuchMethodException {
        dest.setIntProperty(1);
        orig.setIntProperty(0);
        copier.copyProperties(dest, orig);
        assertEquals(0, dest.getIntProperty());
    }

}

package com.pijam.application.utils;

public class TestBean {
    private String stringProperty;
    private int intProperty;

    public String getStringProperty() {
        return stringProperty;
    }
    public TestBean setStringProperty(String stringProperty) {
        this.stringProperty = stringProperty;
        return this;
    }
    public int getIntProperty() {
        return intProperty;
    }
    public TestBean setIntProperty(int intProperty) {
        this.intProperty = intProperty;
        return this;
    }
}